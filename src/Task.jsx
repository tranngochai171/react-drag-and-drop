import { Draggable } from "react-beautiful-dnd";
import styled from "styled-components";

const Container = styled.div`
	border: 1px solid lightgray;
	padding: 8px;
	border-radius: 2px;
	margin-bottom: 8px;
	background-color: ${(props) =>
		props.isDragDisabled
			? "lightgray"
			: props.isDragging
			? "lightgreen"
			: "#fff"};
	display: flex;
`;

const Handle = styled.div`
	width: 20px;
	height: 20px;
	background-color: orange;
	border-radius: 4px;
	margin-right: 8px;
`;

const Task = ({ task, index }) => {
	const isDragDisabled = task.id === "task-1";
	return (
		<Draggable
			isDragDisabled={isDragDisabled}
			draggableId={task.id}
			index={index}
		>
			{(provided, snapshot) => (
				<Container
					{...provided.draggableProps}
					ref={provided.innerRef}
					isDragging={snapshot.isDragging}
					isDragDisabled={isDragDisabled}
				>
					<Handle {...provided.dragHandleProps} />
					{task.content}
				</Container>
			)}
		</Draggable>
	);
};

export default Task;
