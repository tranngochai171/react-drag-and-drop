import React, { useState } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import * as ReactDOM from "react-dom";
import Column from "./Column";
import initialData from "./initial-data";
import styled from "styled-components";

const Container = styled.div`
	display: flex;
`;

const App = () => {
	const [value, setValue] = useState(initialData);
	const [homeIndex, setHomeIndex] = useState(null);

	const onDragStart = (start) => {
		if (start.type === "task") {
			setHomeIndex(value.columnOrder.indexOf(start.source.droppableId));
		}
	};

	const onDragEnd = (result) => {
		console.log(result);
		const { destination, source, draggableId, type } = result;
		if (type === "task") {
			setHomeIndex(null);
			if (!destination) {
				return;
			}

			if (
				destination.droppableId === source.droppableId &&
				destination.index === source.index
			) {
				return;
			}
			const oldColumn = { ...value.columns[source.droppableId] };
			const newColumn = { ...value.columns[destination.droppableId] };

			oldColumn.taskIds.splice(source.index, 1);
			newColumn.taskIds.splice(destination.index, 0, draggableId);
			setValue((state) => ({
				...state,
				columns: { ...state.columns, oldColumn, newColumn },
			}));
			return;
		}
		const newColumnOrder = [...value.columnOrder];
		newColumnOrder.splice(source.index, 1);
		newColumnOrder.splice(destination.index, 0, draggableId);
		setValue((state) => ({
			...state,
			columnOrder: newColumnOrder,
		}));
	};

	return (
		<DragDropContext onDragEnd={onDragEnd} onDragStart={onDragStart}>
			<Droppable
				droppableId="all-columns"
				direction="horizontal"
				type="column"
			>
				{(provided) => (
					<Container
						{...provided.droppableProps}
						ref={provided.innerRef}
					>
						{value.columnOrder.map((columnId, index) => {
							const column = value.columns[columnId];
							const tasks = column.taskIds.map(
								(taskId) => value.tasks[taskId]
							);
							const isDropDisabled = homeIndex > index;
							return (
								<Column
									key={column.id}
									column={column}
									tasks={tasks}
									isDropDisabled={isDropDisabled}
									index={index}
								/>
							);
						})}
						{provided.placeholder}
					</Container>
				)}
			</Droppable>
		</DragDropContext>
	);
};
ReactDOM.render(<App />, document.getElementById("root"));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
